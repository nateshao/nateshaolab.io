---
title: day2-docker+ovs+pipework配置容器ip互通
date: 2023-06-03 23:43:25
tags:
- 集群
- sre
- devops
- docker
---

<img src="https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230604002333.jpg" alt="docker+ovs+pipework配置容器ip互通" style="zoom:200%;" />

# docker+ovs+pipework配置容器ip互通

目的：两个宿主机里面的容器可以互通。

操作思路：

1. ping自己的网卡
2. ping对方的网卡
3. ping对方的容器ip
4. done

> 前提：需要两台服务器。以10.8.46.197和10.8.46.190为例子

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230604000300.png)

## 1.安装依赖包：

```sh
yum -y update
yum -y install net-tools 
yum -y install bridge-utils
yum -y install make gcc openssl-devel autoconf automake rpm-build redhat-rpm-config  
yum -y install python-devel openssl-devel kernel-devel kernel-debug-devel libtool wget  
yum -y install selinux-policy-devel
yum -y install python-six
yum -y install wget
```

## 2.预处理：

```sh
cd ~ && mkdir -p ~/rpmbuild/SOURCES && mkdir /etc/openvswitch
wget http://openvswitch.org/releases/openvswitch-2.7.0.tar.gz  
cp openvswitch-2.7.0.tar.gz ~/rpmbuild/SOURCES/  
tar xfz openvswitch-2.7.0.tar.gz  
sed 's/openvswitch-kmod, //g' openvswitch-2.7.0/rhel/openvswitch.spec > openvswitch-2.7.0/rhel/openvswitch_no_kmod.spec 
```

## 3.构建RPM包：

```sh
rpmbuild -bb --nocheck ~/openvswitch-2.7.0/rhel/openvswitch_no_kmod.spec 
```

## 4.安装：

```sh
yum -y localinstall ~/rpmbuild/RPMS/x86_64/openvswitch-2.7.0-1.x86_64.rpm 
```

## 5.启动相关服务：

```sh
setenforce 0
systemctl stop firewalld && systemctl disable firewalld
systemctl start openvswitch.service  
systemctl enable openvswitch.service
systemctl  status openvswitch.service –l
```

安装pipework

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220726093019793.png)

```sh
cd /opt
unzip pipework-master.zip
mv pipework-master pipework
cp -rp pipework/pipework /usr/local/bin/
```

## 6.设置OVS系统开机启动

```sh
mkdir /opt/script
mv autostart.sh script/
```

自启动脚本 （每一台都要装）(该脚本只在重启虚拟机之后使用）

（以192.168.100.122 192.168.100.118为例）

```sh
vim /opt/script/autostart.sh
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230604003024.png)

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230604003006.png)

```sh
chmod +x /opt/script/autostart.sh
```

```sh
将文件目录的路径放入

vim /etc/rc.d/rc.local
/opt/script/autostart.sh
将/opt/script/autostart.sh的路径放入文本中
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230604003204.png)

```sh
chmod +x /etc/rc.d/rc.local


设置关闭dockr的开机自动启动
chkconfig docker off
chmod +x /opt/script/autostart.sh
```

# 创建tomcat容器配置ip

（可以查看ip，命令ip a。以192.168.100.122和192.168.100.118为例）

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220726093606039.png)

## 创建tomcat脚本

服务器1的启动脚本

```sh
#!/bin/bash
#设置容器相关变量
cname="tomcat"
name="jdk1.8"

logs="/opt/data/"${cname}"/"
#创建目录
mkdir -p ${logs}
port1="8080"
#启动容器
echo -e "Start "${cname}" Container"

docker run -d -it -p ${port1}:${port1} --privileged=true --net=none --restart=always --name ${cname} --hostname ${name}  -v ${logs}:/opt/data 10.249.0.137:80/base/jdk-1.8:20210202
pipework br0 tomcat 172.17.197.12/24@172.17.197.1
```

启动脚本

```sh
[root@localhost jdk]# pwd 
/opt/script/setup/jdk
[root@localhost jdk]# ./install.sh
```

重启：reboot

## ping网卡

进入容器，ping自己网卡

```sh
[root@localhost jdk]# docker exec -it tomcat bash
[root@jdk1 /]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
17: eth1@if18: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 7e:7a:a6:28:cf:71 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.197.12/24 brd 172.17.197.255 scope global eth1
       valid_lft forever preferred_lft forever
[root@jdk1 /]# ping 172.17.197.1
PING 172.17.197.1 (172.17.197.1) 56(84) bytes of data.
64 bytes from 172.17.197.1: icmp_seq=1 ttl=64 time=1.28 ms
64 bytes from 172.17.197.1: icmp_seq=2 ttl=64 time=0.090 ms
64 bytes from 172.17.197.1: icmp_seq=3 ttl=64 time=0.060 ms
64 bytes from 172.17.197.1: icmp_seq=4 ttl=64 time=0.067 ms
64 bytes from 172.17.197.1: icmp_seq=5 ttl=64 time=0.082 ms
^C
--- 172.17.197.1 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4001ms
rtt min/avg/max/mdev = 0.060/0.316/1.282/0.483 ms
[root@jdk1 /]#
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220726094437990.png)

同样操作，将测试服务器10.8.46.190也操作一次。

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220726094605284.png)

效果：各自ping对方的容器

- 虚拟机容器互通

- 验证网桥：进入每台机的容器去ping同一台机的虚拟机ip

- 注意：**如果其他主机内的容器不能ping同该容器，而相同主机的容器能ping通，请检查创建脚本是否忘记添加网关的ip**

| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220726095613476.png) |
| :----------------------------------------------------------: |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220726095634814.png) |

---

## 八股文

## docker 四种网络模型

我们在使用docker run创建Docker容器时，可以用`--net`选项指定容器的网络模式，Docker有以下4种网络模式

- host模式，使用`--net=host`指定。
- container模式，使用`--net=container:NAME_or_ID`指定。
- none模式，使用`--net=none`指定。
- bridge模式，使用`--net=bridge`指定，默认设置。



| 网络模式      | 配置                    | 说明                                                         |
| ------------- | ----------------------- | ------------------------------------------------------------ |
| bridge模式    | --net=bridge            | 默认值，在Docker网桥docker0上为容器创建新的网络栈            |
| none模式      | --net=none              | 不配置网络，用户可以稍后进入容器，自行配置                   |
| container模式 | --net=container:name/id | 容器和另外一个容器共享Network namespace。kubernetes中的pod就是多个容器共享一个Networknamespace. |
| host模式      | --net=host              | 容器和宿主机共享Network namespace                            |
| 用户自定义    | --net=自定义网络        | 用户自己使用network相关命令定义网络，创建容器的时候可以指定为自己定义的网络 |

参考文献：
1. docker的4种网络模型：https://zhuanlan.zhihu.com/p/559977397

​	2. Docker+Ovs构建SDN网络：https://blog.csdn.net/weixin_40042248/article/details/120390840
