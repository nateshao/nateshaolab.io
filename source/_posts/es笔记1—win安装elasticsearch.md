---
title: es笔记1—win安装elasticsearch
date: 2023-11-07 10:17:38
tags: elasticsearch
---

[TOC]

背景：自从上一篇慢sql改为单表优化之后，其实最终还是要将数据同步到elasticsearch，进行elasticsearch查询。包括之前搜推资源列表也需要用elasticsearch重构mysql查询

# windows安装elasticsearch(保姆教程)

前置要求：**需要elasticsearch和Kibana，分词器，jdk版本保持一致**。不然es可能跑不起来。

这里是采用

- jdk1.8
- elasticsearch-7.17.5
- Kibana 7.17.5
- 分词器：elasticsearch-analysis-ik-7.17.5
- https://github.com/medcl/elasticsearch-analysis-ik/releases?page=12

## 一、安装jdk

ElasticSearch是基于lucence开发的，需要Java环境。所以先保证本地有安装jdk环境。

jdk下载地址：https://www.oracle.com/java/technologies/downloads/#java8-windows

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107102020045.png)

由于ElasticSearch 5.x 往后依赖于JDK 1.8的，所以现在我们下载JDK 1.8或者更高版本。

## 二、安装ElasticSearch

**文件扩展名记得打开**

1. ElasticSearch下载地址：https://www.elastic.co/cn/downloads/past-releases/elasticsearch-7-17-5
2. 下载安装加压当前文件夹

3. 进入bin目录下，**双击执行elasticsearch.bat**
   1. ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107213028193.png)


4. 看到started说明启动成功了，打开浏览器http://localhost:9200访问

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107103448358.png)

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107212917788.png)

## 三、插件elasticsearch-head 安装

前提，安装node等环境

1. node环境

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107103736216.png)

2. 下载elasticsearch-head插件

	1. ```sh
       git clone git clone git://github.com/mobz/elasticsearch-head.git
   
   2. 安装grunt-cli
	
      ```sh
      npm install -g grunt-cli
      ```
   
      grunt是一个很方便的构建工具，可以进行打包压缩、测试、执行等等的工作，5.x里之后的head插件就是通过grunt启动的。因此需要安装grunt.
   
   3. 安装 grunt
   
      elasticsearch-head 下载完成后，进入 elasticsearch-head 文件夹，执行命令：
   
      ```sh
      npm install grunt --save
      ```
   
   4. 安装依赖的 npm 包
   
      1. ```sh
         npm install
         ```
   
   5. 查看版本号 grunt -version
   
   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107103907357.png)
   
   6. 修改文件elasticsearch-head 目录下的 Gruntfile.js，在options属性下添加hostname，设置为 0.0.0.0。
   
      ```sh
      		connect: {
      			server: {
      				options: {
      					hostname: '0.0.0.0',
      					port: 9100,
      					base: '.',
      					keepalive: true
      				}
      			}
      		}
      ```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107104927801.png)

7. 修改 Elasticsearch 配置文件 config/elasticsearch.yml

   在配置文件最后增加两个配置项，这样 elasticsearch-head 插件才可以访问 Elasticsearch 。

   ```css
   http.cors.enabled: true
   http.cors.allow-origin: "*"
   ```

8. 运行elasticsearch-head

   **npm run start或grunt server**，启动head插件

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107105400290.png)

9. 访问：http://localhost:9100/，就可以看到当前 Elasticsearch 集群信息。

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107211511135.png)

## 四、elasticsearch安装为windows服务

1.elasticsearch的bin目录下有一个elasticsearch-service.bat

2.cmd 进入bin目录下执行: elasticsearch-service.bat install

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107105805976.png)

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107105737296.png)

## 五、启动Kibana

1. 下载并解压Kibana安装包：https://www.elastic.co/cn/downloads/past-releases/kibana-7-17-10

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107112758460.png)

2. 双击kibana.bat，访问：http://localhost:5601

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107112908952.png)

3. 打开Dev Tools，进行愉快的elasticsearch查询啦

   http://localhost:5601/app/dev_tools#/console

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107113459564.png)

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107113522382.png)

4. 分词器安装：

   - 分词器：elasticsearch-analysis-ik v6.5.4 

   - https://github.com/medcl/elasticsearch-analysis-ik/releases?page=12

分词器演示：

```sh
GET /_analyze
{
  "text": "程序员千羽",
  "analyzer": "ik_smart"
}
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/wximage-20231107211621741.png)



到此为止，es基本环境安装完成。接下来就是查询操作了。

elastic官方文档：https://www.elastic.co/guide/cn/elasticsearch/guide/current/intro.html

链接：https://www.jianshu.com/p/e3f7bf6277f9

https://www.cnblogs.com/hualess/p/11540477.html
