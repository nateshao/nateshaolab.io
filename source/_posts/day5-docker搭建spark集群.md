---
title: day5-docker搭建spark集群
date: 2023-06-11 08:24:30
tags:
- 集群
- sre
- devops
- docker
---

[TOC]

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230611084504.jpeg)

**系列文章**

1. [使用docker+nginx实现一个简单的负载均衡](http://mp.weixin.qq.com/s?__biz=MzIyNjE0MDI1NQ==&mid=2247490721&idx=1&sn=75d3005d112ee9275331c0abc42ba48b&chksm=e8745591df03dc87711cdb9135da5e1f9a3d74110522b276df7c18b976ef1557a730268aa35c&scene=21#wechat_redirect)
2. [docker+ovs+pipework配置容器ip互通](http://mp.weixin.qq.com/s?__biz=MzIyNjE0MDI1NQ==&mid=2247490801&idx=1&sn=30f67800f76c9c0adb7bebdc71e035c1&chksm=e87455c1df03dcd7523874ca03607ff15c018516f3ca09544f33db9ac4661f414de6cbdffe94&scene=21#wechat_redirect)
3. [docker搭建zookeeper集群](http://mp.weixin.qq.com/s?__biz=MzIyNjE0MDI1NQ==&mid=2247490847&idx=1&sn=8a47061d43891f55fba132fbae40d9e0&chksm=e874542fdf03dd39d6a435ea94ab25811bab3ecd32b5bedb60f046197703f80f30c2e63a524e&scene=21#wechat_redirect)
4. [docker搭建hadoop集群](http://mp.weixin.qq.com/s?__biz=MzIyNjE0MDI1NQ==&mid=2247490915&idx=1&sn=7926141c96494a3febc7d9cd5c735893&chksm=e8745453df03dd4567e32ebb4a12edb9e229615f2d30dbac27ace8a79d73dc2c83f5f21cfb4f#rd)

## docker搭建spark集群

有个小技巧：先配置好一个，在（宿主机上）复制 `scp -r` 拷贝Spark到其他Slaves。

## 1.安装配置基础Spark

【在test-cluster-hap-master-01虚拟主机上】

将已下载好的Spark压缩包（spark-3.1.1-bin-hadoop-3.2.2-lbx-jszt.tgz）通过工具【XFtp】拷贝到虚拟主机的opt目录下：

## 2.通过脚本挂起镜像

```sh
cd   /opt/script/setup/spark 
```

### test-cluster-spk-master-01

```sh
#!/bin/bash 
#编写作者：程序员千羽

cname="test-cluster-spk-master-01"

#port1="8080"
#port2="7077"
log="/opt/data/"${cname}
images="10.249.0.137:80/base/jdk-1.8:20210202"

mkdir -p ${log}
mkdir ${log}/logs
mkdir ${log}/work
mkdir ${log}/data
mkdir ${log}/jars

# docker run -d --net=overlay-net --ip ${ip} -p ${port1}:${port1} -p ${port2}:${port2} --name ${cname} --hostname ${cname} --privileged=true --restart=always 
docker run -d --net=host --name ${cname} --hostname ${cname} --privileged=true --restart=always \
-v ${log}/logs:/usr/local/spark-3.1.1/logs \
-v ${log}/work:/usr/local/spark-3.1.1/work \
-v ${log}/jars:/usr/local/spark-3.1.1/jars \
-v ${log}/data:/opt/data \
${images} \
/usr/sbin/init
```

### test-cluster-spk-master-02

```sh
#!/bin/bash 
cname="test-cluster-spk-master-02"

#port1="8080"
#port2="7077"
log="/opt/data/"${cname}
images="10.249.0.137:80/base/jdk-1.8:20210202"

mkdir -p ${log}
mkdir ${log}/logs
mkdir ${log}/work
mkdir ${log}/data
mkdir ${log}/jars

#docker run -d --net=overlay-net --ip ${ip} -p ${port1}:${port1} -p ${port2}:${port2} --name ${cname} --hostname ${cname} --privileged=true --restart=always 
docker run -d --net=host --name ${cname} --hostname ${cname} --privileged=true --restart=always \
-v ${log}/logs:/usr/local/spark-3.1.1/logs \
-v ${log}/work:/usr/local/spark-3.1.1/work \
-v ${log}/jars:/usr/local/spark-3.1.1/jars \
-v ${log}/data:/opt/data \
${images} \
/usr/sbin/init
```

### test-cluster-spk-slave-01

```sh
#!/bin/bash 
cname="test-cluster-spk-slave-01"

#port1="8080"
#port2="7077"
log="/opt/data/"${cname}
images="10.249.0.137:80/base/jdk-1.8:20210202"

mkdir -p ${log}
mkdir ${log}/logs
mkdir ${log}/work
mkdir ${log}/data
mkdir ${log}/jars

#docker run -d --net=overlay-net --ip ${ip} -p ${port1}:${port1} -p ${port2}:${port2} --name ${cname} --hostname ${cname} --privileged=true --restart=always 
docker run -d --net=host --name ${cname} --hostname ${cname} --privileged=true --restart=always \
-v ${log}/logs:/usr/local/spark-3.1.1/logs \
-v ${log}/work:/usr/local/spark-3.1.1/work \
-v ${log}/jars:/usr/local/spark-3.1.1/jars \
-v ${log}/data:/opt/data \
${images} \
/usr/sbin/init
```

```sh
[root@zookeeper-03-test spark]# ll
总用量 4
-rw-r--r--. 1 root root 1166 7月  28 17:44 install.sh
[root@zookeeper-03-test spark]# chmod +x install.sh 
[root@zookeeper-03-test spark]# ll
总用量 4
-rwxr-xr-x. 1 root root 1166 7月  28 17:44 install.sh
[root@zookeeper-03-test spark]#
```

## 3.上传spark安装包

**在容器映射目录下** ：/opt/data/test-cluster-spk-slave-01/data

```sh
[root@hadoop-01 data]# pwd
/opt/data
```

用Xftp上传包

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729143314408.png)

**这里需要上传两个，使用的是spark-3.1.1-bin-without-hadoop.tgz**

**但是需要将spark-3.1.1-bin-hadoop-3.2.2-lbx-jszt下的jars包移到/usr/local/spark-3.1.1/jars下**

## 4.解压安装包

```sh
mkdir -p /usr/local/spark-3.1.1
cd /opt/data
tar -zxvf spark-3.1.1-bin-without-hadoop.tgz -C /usr/local/spark-3.1.1
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729091830912.png)

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729091847484.png)

---

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729135621087.png)

编辑全局变量

```sh
vim /etc/profile
```

增加以下全局变量

```sh
export SPARK_HOME=/usr/local/spark-3.1.1			
export PATH=$PATH:$SPARK_HOME/bin
```

即时生效

```sh
source /etc/profile
```

## 5.配置spark-env.sh

```sh
cd /usr/local/spark-3.1.1/conf
cp spark-env.sh.template spark-env.sh
vim spark-env.sh
```

```sh
export SPARK_MASTER_IP=test-cluster-spk-master-01
export SPARK_WORKER_CORES=1
export SPARK_WORKER_MEMORY=800m
#export SPARK_DRIVER_MEMORY=4g
export SPARK_EXECUTOR_INSTANCES=2
export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
export SPARK_LOCAL_DIRS=/home/hadoop/tmp/spark/tmp

#定时清理worker文件 一天一次
export SPARK_WORKER_OPTS="  
-Dspark.worker.cleanup.enabled=true  
-Dspark.worker.cleanup.interval=86400 
-Dspark.worker.cleanup.appDataTtl=86400"

export JAVA_HOME=/usr/local/jdk1.8
export HADOOP_HOME=/usr/local/hadoop
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export SCALA_HOME=/usr/local/scala
export PATH=${SCALA_HOME}/bin:$PATH
export SPARK_DAEMON_JAVA_OPTS="-Dspark.deploy.recoveryMode=ZOOKEEPER -Dspark.deploy.zookeeper.url=zookeeper-01-test:2181,zookeeper-02-test:2181,zookeeper-03-test:2181 -Dspark.deploy.zookeeper.dir=/usr/local/spark"

```

（4）配置workers

```sh
cp workers.template workers
vim workers
# 添加
test-cluster-spk-slave-001
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220728155304848.png)

## 6.配置log4j.properties

```sh
cp log4j.properties.template log4j.properties
vim log4j.properties

log4j.rootCategory=WARN, console
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220728155405768.png)

## 7.复制到其他slave

（宿主机上）复制scp -r拷贝Spark到其他Slaves节点：

```sh
scp -r /usr/local/spark/spark-2.1.0-bin-hadoop2.7 root@slave-001-spark-dev:/usr/local/spark/

scp -r /usr/local/spark/spark-2.1.0-bin-hadoop2.7 root@slave-002-spark-dev:/usr/local/spark/

scp -r /usr/local/spark/spark-2.1.0-bin-hadoop2.7 root@slave-003-spark-dev:/usr/local/spark/
```

如执行命令出现出现问题时，请现在相应的Slave节点执行mkdir -p /usr/local/spark

复制到master-02时，使用start-mater.sh启动master-02

## 8.启动spark

1. 先启动两个`master`，然后启动`slave`节点

   1. 主节点1启动完成

   ```sh
   [root@test-cluster-spk-master-01 sbin]# ./start-master.sh 
   starting org.apache.spark.deploy.master.Master, logging to /usr/local/spark-3.1.1/logs/spark-root-org.apache.spark.deploy.master.Master-1-test-cluster-spk-master-01.out
   [root@test-cluster-spk-master-01 sbin]# jps
   548 Jps
   492 Master
   [root@test-cluster-spk-master-01 sbin]# pwd
   /usr/local/spark-3.1.1/sbin
   [root@test-cluster-spk-master-01 sbin]#
   ```

   ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729144110808.png)

   1. 主节点2启动完成

      ```sh
      [root@test-cluster-spk-master-02 sbin]# ./start-master.sh 
      starting org.apache.spark.deploy.master.Master, logging to /usr/local/spark-3.1.1/logs/spark-root-org.apache.spark.deploy.master.Master-1-test-cluster-spk-master-02.out
      [root@test-cluster-spk-master-02 sbin]# pwd
      /usr/local/spark-3.1.1/sbin
      [root@test-cluster-spk-master-02 sbin]# jps
      274 Jps
      218 Master
      [root@test-cluster-spk-master-02 sbin]#
      ```

      ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729144050441.png)

   2. 从节点启动完成

      ```sh
      /usr/local/spark-3.1.1/sbin/start-slave.sh test-cluster-hap-slave-001 test-cluster-hap-master-02:7077,test-cluster-hap-master-02:7077
      ```

      ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729151337284.png)

## 9.验证

原本是访问http://10.8.46.35:8080 就可，但是我这里在配置镜像的时候，多了8080，导致这里访问不了。看日志可以知道，已经走向8081

所以http://10.8.46.35:8081/即可	

|                           主节点1                            |                          停掉主节点                          |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729152159744.png) | ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729152944772.png)![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729153011920.png) |
|                           主节点2                            |                       从节点成为ALIVE                        |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729152302979.png) | ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729153059107.png) |
|                           从节点1                            |                           从节点1                            |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729152356443.png) | ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729153140693.png) |

---

## 10.遇到的坑

**包不兼容**

这里遇到了许多问题，第一个是包不兼容，导致搭建两次失败

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729153836769.png)

然后换了官方的包`spark-3.1.1-bin-without-hadoop`，启动还是有问题。

最后通过替换jars才成功。（使用`spark-3.1.1-bin-hadoop-3.2.2-lbx-jszt`下的jars）

**ctrl + p + q    从容器退出到宿主机**
