---
title: Java性能分析之火焰图
tags: 软件测试
abbrlink: 8696
date: 2020-09-03 23:14:47
---

## 火焰图来了

> Java性能分析火焰图是什么？

1. `java`性能分析火焰图的所做的事情就是能够分析出`java`程序运行期间存在的性能问题，因为某段代码拖慢整个程序执行是不允许的，因此靠火焰图的绘制和分析就可以找出类似的“问题代码段”。

2. 那么这个图是怎么来的呢？首先跟大多数监控系统一样，数据采集+前端绘图，这个图也是根据某些数据绘制而成的，绘图工具本篇文章采用[FlameGraph](https://github.com/brendangregg/FlameGraph)，而负责收集这些数据的工具，这里采用`async-profiler`，这个工具会在程序运行期间向`jvm`发送信号采集其运行期数据（简单来说就是通过该工具可以找出程序中占用CPU资源时间最长的代码块，这里`async-profiler`的实现使用了`jvmti`，然后生成相应的数据格式文件，而[FlameGraph](https://github.com/brendangregg/FlameGraph)则负责读取和解析数据文件生成对应的火焰图（svg文件）。

## 从GitHub上clone下来async-profiler

git clone https://github.com/jvm-profiling-tools/async-profiler 

这里使用mac演示

可以新建一个工程，编写`Target.java`

```java
import java.io.File;

public class Target {
    private static volatile int value;   
private static void method1() {
        for (int i = 0; i < 1000000; ++i) {
            ++value;
        }
    }

    private static void method2() {
        for (int i = 0; i < 1000000; ++i) {
            ++value;
        }
    }

    private static void method3() throws Exception {
        for (int i = 0; i < 1000; ++i) {
            for (String s : new File("/tmp").list()) {
                value += s.hashCode();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        while (true) {
            method1();
            method2();
            method3();
        }
    }
}
```
打开终端：输入`jps`

![](https://cdn.jsdelivr.net/gh/nateshao/images/20220331205516.png)



输入命令，回车

![](https://cdn.jsdelivr.net/gh/nateshao/images/20220331205523.png)



生成svg

![](https://cdn.jsdelivr.net/gh/nateshao/images/20220331205528.png)



选择谷歌浏览器打开

![生成火焰图](https://cdn.jsdelivr.net/gh/nateshao/images/20220331205533.png)

接下来就可以慢慢进行性能分析了