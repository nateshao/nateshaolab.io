---
title: day6-docker搭建HBase集群
date: 2023-06-11 12:52:24
tags:
- 集群
- sre
- devops
- docker
---

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230611125859.jpg)

[TOC]



## 0.准备工作

> `HBase`是依赖`Hadoop`的，所以`hapoop`不要停。

先停掉`spark`集群，减少资源占用情况.

在宿主机上搭建

## 1.修改主机名

```sh
hostnamectl set-hostname master-01-hbase-test
```

修改/etc/hosts

```sh
vim /etc/hosts
```

```sh
10.8.46.35      master-01-hbase-test
10.8.46.197     master-02-hbase-test
10.8.46.190     slave-01-hbase-test
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729171450173.png)

## 2.解压hbase安装包

把二进制包`hbase-2.4.4-bin.tar.gz`上传，解压

```sh
mkdir -p /usr/local/hbase
cd /opt
tar -zxvf hbase-2.4.4-bin.tar.gz -C /usr/local/hbase
```

## 3.编辑全局变量

```sh
vim /etc/profile

增加以下全局变量
export HBASE_HOME=/usr/local/hbase/hbase-2.4.4
export PATH=$PATH:$HBASE_HOME/bin
export HBASE_HOME PATH SPARK_HOME SCALA_HOME
```

#即时生效

```sh
source /etc/profile
```

## 4.配置hbase-env.sh

```sh
cd /usr/local/hbase/hbase-2.4.4/conf

vim hbase-env.sh
export HBASE_MANAGES_ZK=false
export JAVA_HOME=/usr/local/jdk1.8
```

## 5.配置hbase-site.xml

```sh
vim hbase-site.xml
```

```xml
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<configuration>
<!--HBase数据目录位置                  ?-->
<property>
   <name>hbase.rootdir</name>
   <value>hdfs://master-01-spark-test:9000/hbase</value>
</property>
<!--启用分布式集群-->
<property>
   <name>hbase.cluster.distributed</name>
   <value>true</value>
</property>
<!--默认HMaster HTTP访问端口-->
<property>
   <name>hbase.master.info.port</name>
   <value>16010</value>
</property>
<!--默认HRegionServer HTTP访问端口-->
<property>
   <name>hbase.regionserver.info.port</name>
   <value>16030</value>
</property>
<!--不使用默认内置的，配置独立的ZK集群地址-->
<property>
   <name>hbase.zookeeper.quorum</name>
   <value>zookeeper-01-test,zookeeper-02-test,zookeeper-03-test</value>
</property>
</configuration>
```

## 6.配置regionservers

```sh
vim regionservers

slave-01-hbase-test
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230611130425.png)

将这两个文件放进去

```sh
[root@master-01-hbase-test conf]# pwd
/usr/local/hbase/hbase-2.4.4/conf
[root@master-01-hbase-test conf]# 
```

![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729181650182.png)

启动HBase集群

Master节点：/usr/local/hbase/hbase-2.4.4/bin/start-hbase.sh

Slave节点：/usr/local/hbase/hbase-2.4.4/bin/hbase-daemon.sh start regionserver

## 7.验证

hbase集群
主节点1：http://10.8.46.35:16010/master-status#alltasks
主节点2：http://10.8.46.197:16010/master-status#alltasks
从节点1：http://10.8.46.190:16030/rs-status

|                           主节点1                            |
| :----------------------------------------------------------: |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/image-20220729181123587.png) |
|                           主节点2                            |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230611130218.png) |
|                           从节点1                            |
| ![](https://nateshao-blog.oss-cn-shenzhen.aliyuncs.com/img/20230611130227.png) |

​	

































